#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#define DOT 0
#define KISS 1

using namespace std;

int outputState = DOT;
int inputAmount;

string DecimalToBinary(int decimal_number) {
	string res = "";
	if (inputAmount == 1) {
		if (decimal_number == 0)res = "0";
		else if (decimal_number == 1)res = "1";
	}
	else if (inputAmount == 2) {
		if (decimal_number == 0)res = "00";
		else if (decimal_number == 1)res = "01";
		else if (decimal_number == 2)res = "10";
		else if (decimal_number == 3)res = "11";
	}
	return res;
}

int BinaryToDecimal(string binary_number) {
	string str = binary_number;
	int res = 0;
	int base = 1;
	for (int i = str.length() - 1; i >= 0; i--) {
		if (str[i] == '1')res += base;
		base = base * 2;
	}
	return res;
}

class term {
private:
	string statename;
	vector<string> nextState;
	vector<int> output;
public:
	term() {
		statename = "";
	}

	term(string input, string statename, string nextState, int output) {
		this->statename = statename;
		this->nextState.push_back(nextState);
		this->output.push_back(output);
	}

	string getStatename(void) { return statename; }

	friend ostream& operator<< (ostream& ostrm, term& term) {
		if (outputState == DOT) {
			vector<string> sequence;
			map<string, vector<int>> outNextstate;
			for (int i = 0; i < term.nextState.size(); i++) {
				if (!outNextstate.count(term.nextState[i])) {
					vector<int> temp;
					temp.push_back(i);
					outNextstate.insert({ term.nextState[i] , temp });
					sequence.push_back(term.nextState[i]);
				}
				else {
					outNextstate[term.nextState[i]].push_back(i);
				}
			}

			for (int i = 0; i < sequence.size(); i++) {
				vector<int> temp = outNextstate[sequence[i]];
				ostrm << "\t" << term.statename << " -> " << sequence[i] << "[label=\"";
				for (int j = 0; j < temp.size(); j++) {
					if (j == temp.size() - 1) ostrm << DecimalToBinary(temp[j]) << "/" << term.output[temp[j]] << "\"];" << endl;
					else ostrm << DecimalToBinary(temp[j]) << "/" << term.output[temp[j]] << ",";
				}
			}
		}
		else if (outputState == KISS) {
			for (int i = 0; i < term.nextState.size(); i++) {
				ostrm << DecimalToBinary(i) << " " << term.statename << " " << term.nextState[i] << " " << term.output[i] << endl;
			}
		}
		return ostrm;
	}

	bool checkAndSet(string input, string statename, string nextState, int output) {
		bool check = false;
		if (statename != this->statename) return check;
		else {
			this->nextState.push_back(nextState);
			this->output.push_back(output);
			check = true;
		}
		return check;
	}

	string getNextState(string input) {
		int index = BinaryToDecimal(input);
		return nextState[index];
	}

	int nextStateSize(void) { return nextState.size(); }

	int getOutput(string input) {
		int index = BinaryToDecimal(input);
		return output[index];
	}

	void replaceState(string originalState, string replaceState) {
		for (int i = 0; i < nextState.size(); i++) {
			if (nextState[i] == originalState) nextState[i] = replaceState;
		}
		return;
	}

	bool compareOutput(term* term) {
		if (output.size() != term->output.size())return false;
		for (int i = 0; i < output.size(); i++) {
			if (output[i] != term->output[i])return false;
		}
		return true;
	}
};

class ImplicationTable {
	struct box {
		vector<string> nextState;
		bool compatible;

		box() :compatible(true) {}

		void insertState(vector<string> States) {
			nextState = States;
		}
		friend ostream& operator<< (ostream& ostrm, box& box) {
			if (box.compatible) {
				for (int i = 0; i < inputAmount * 4; i += 2) {
					ostrm << "[" << box.nextState[i] << "-" << box.nextState[i + 1] << "]";
				}
			}
			else {
				for (int i = 0; i < inputAmount * 2; i++) ostrm << "[x-x]";
			}
			return ostrm;
		}
	};
private:
	int size;
	int statesAmount;
	vector<vector<box>> table;
	map<string, term*> stateMap;
	vector<pair<string, term*>> states;
	map<string, int> stateIndex;
	vector<string> stateSequence;
	bool minimized;

public:
	ImplicationTable() {
		size = 0;
		statesAmount = 0;
		minimized = false;
	}

	bool isMinimized(void) { return minimized; }

	void initialize(map<string, term*> stateMap, vector<string> stateSequence) {
		int index = 0;
		this->stateMap = stateMap;
		this->statesAmount = stateMap.size();
		this->stateSequence = stateSequence;

		for (int i = 0; i < stateSequence.size(); i++) {
			states.push_back({ stateSequence[i], stateMap[stateSequence[i]] });
			stateIndex.insert({ stateSequence[i], index });
			index++;
		}

		for (int j = 0; j < statesAmount; j++) {
			vector<box> row;
			for (int i = 0; i < statesAmount; i++) {
				if (j >= 1 && i < j) {
					box temp;
					vector<string> nextStates;

					for (int s = 0; s < states[j].second->nextStateSize(); s++) {
						string input = DecimalToBinary(s);
						nextStates.push_back(states[j].second->getNextState(input));
						nextStates.push_back(states[i].second->getNextState(input));
					}

					temp.insertState(nextStates);
					row.push_back(temp);
				}
				else {
					box temp;
					temp.compatible = false;
					row.push_back(temp);
				}
			}
			table.push_back(row);
		}
		//printTable();
	}

	void doStateMini(void) {
		//printTable();
		removeBoxByOutput();
		//printTable();
		updateTable();
		//printTable();
		MergeCompatibleStates();
		//printTable();
	}

	void removeBoxByOutput(void) {
		for (int j = 0; j < table.size(); j++) {
			for (int i = 0; i < table[j].size(); i++) {
				if (j >= 1 && i < j) {
					if (!states[j].second->compareOutput(states[i].second)) {
						table[j][i].compatible = false;
						table[i][j].compatible = false;
					}
				}
			}
		}
		//printTable();
	}

	void updateTable(void) {
		bool haveDoneUpdate = false;
		vector<pair<int, int>> markRed;
		for (int j = 0; j < table.size(); j++) {
			for (int i = 0; i < table[j].size(); i++) {
				if (j >= 1 && i < j && table[j][i].compatible == true) {
					bool inCompatible = false;
					for (int s = 0; s < table[j][i].nextState.size(); s += 2) {
						int col = stateIndex[table[j][i].nextState[s]];
						int row = stateIndex[table[j][i].nextState[s + 1]];
						if (col >= 1 && row < col) {
							if (!isCompatible(col, row))inCompatible = true;
						}
						if (row >= 1 && col < row) {
							if (!isCompatible(row, col))inCompatible = true;
						}
					}
					if (inCompatible)markRed.push_back({ j,i });
				}

			}
		}

		for (int s = 0; s < markRed.size(); s++) {
			int j = markRed[s].first;
			int i = markRed[s].second;
			table[j][i].compatible = false;

			if (!haveDoneUpdate)haveDoneUpdate = true;
		}

		//printTable();

		if (haveDoneUpdate)updateTable();
		else return;
	}

	void MergeCompatibleStates(void) {
		map<string, string> deleteStates;
		map<string, term*> new_stateMap;
		vector<string> new_stateSequence;

		for (int j = 0; j < table.size(); j++) {
			for (int i = 0; i < table[j].size(); i++) {
				if (j >= 1 && i < j && table[j][i].compatible == true) {
					string statename = states[j].first;
					string pairStatename = states[i].first;

					deleteStates.insert({ statename , pairStatename });

					int col = j;
					for (int s = 0; s < table[col].size(); s++) {
						if (table[col][s].compatible == true) table[col][s].compatible = false;
					}
					int row = j;
					for (int s = 0; s < table.size(); s++) {
						if (table[s][row].compatible == true) table[s][row].compatible = false;
					}

				}
			}
		}

		/*for (const auto& state : deleteStates) {
			cout << "deleteState: " << state.first << "->" << state.second << endl;
		}*/
		for (int i = 0; i < stateSequence.size(); i++) {
			bool findState = deleteStates.count(stateSequence[i]);

			if (!findState) {
				for (const auto& deleteState : deleteStates) {
					stateMap[stateSequence[i]]->replaceState(deleteState.first, deleteState.second);
				}
				new_stateMap.insert({ stateSequence[i], stateMap[stateSequence[i]] });
				new_stateSequence.push_back(stateSequence[i]);
			}
		}

		this->stateMap = new_stateMap;
		this->stateSequence = new_stateSequence;

		//for (const auto& terms : stateMap) cout << *terms.second;
	}

	void printTable(void) {
		for (int j = 0; j < table.size(); j++) {
			for (int i = 0; i < table[j].size(); i++) {
				if (j >= 1 && i < j)cout << table[j][i];
			}
			cout << endl;
		}
	}

	bool isCompatible(int j, int i) { if (j >= 0 && j < states.size() && i >= 0 && i < states.size())return table[j][i].compatible; }

	map<string, term*> getStateMap(void) { return this->stateMap; }

	vector<string> getStateSequence(void) { return this->stateSequence; }
};


int main(int argc, char** argv) {
	/*if (1) {
		ifstream infile("case5.kiss");
		ofstream outfile_kiss("out1.kiss");
		ofstream outfile_dot("out1.dot");
		ofstream infile_dot;
		infile_dot.open("input.dot");*/
		if (argc == 4) {
			ifstream infile(argv[1]);
			ofstream outfile_kiss(argv[2]);
			ofstream outfile_dot(argv[3]);
			ofstream infile_dot;
			infile_dot.open("input.dot");

		int inSize = 0;
		int outSize = 0;
		int termSize = 0;
		int stateSize = 0;
		string iniState = "";
		bool start_flag = false;
		vector<term> terms;
		map<string, term*> stateMap;
		ImplicationTable table;
		vector<string> stateSequence;


		string line;
		while (infile >> line) {
			if (line == ".start_kiss") {
				start_flag = true;
				continue;
			}
			else if (line == ".i") {
				infile >> inSize;
				inputAmount = inSize;
			}
			else if (line == ".o") {
				infile >> outSize;
			}
			else if (line == ".p") {
				infile >> termSize;
			}
			else if (line == ".s") {
				infile >> stateSize;
			}
			else if (line == ".r") {
				infile >> iniState;

				for (int i = 0; i < termSize; i++) {
					string input;
					string statename;
					string nextState;
					int output;
					int sequence = 0;

					infile >> input;
					infile >> statename;
					infile >> nextState;
					infile >> output;

					if (stateMap.count(statename)) {
						stateMap[statename]->checkAndSet(input, statename, nextState, output);
					}
					else {
						term* temp = new term(input, statename, nextState, output);
						stateSequence.push_back(statename);
						stateMap.insert({ statename ,temp });
						terms.push_back(*temp);
					}
				}
			}
			else if (line == ".end_kiss") {
				start_flag = false;

				/*for (int i = 0; i < stateSequence.size(); i++) {
					cout << stateSequence[i] << endl;
				}*/

				//input.dot
				outputState = DOT;
				infile_dot << "digraph STG {" << endl;
				infile_dot << "\trankdir = LR;" << endl << endl;
				infile_dot << "\tINIT[shape = point];" << endl;
				for (int i = 0; i < stateSequence.size(); i++) {
					infile_dot << "\t" << stateMap[stateSequence[i]]->getStatename() << "[label = \"" << stateMap[stateSequence[i]]->getStatename() << "\"];" << endl;
				}
				infile_dot << endl;
				infile_dot << "\tINIT -> " << iniState << ";" << endl;
				for (int i = 0; i < stateSequence.size(); i++) {
					infile_dot << *stateMap[stateSequence[i]];
				}
				infile_dot << "}" << endl;
				infile_dot.close();

				//do state minimization
				table.initialize(stateMap, stateSequence);
				table.doStateMini();
				stateMap = table.getStateMap();
				stateSequence = table.getStateSequence();

				//update termSize and stateSize
				stateSize = 0;
				for (const auto& terms : stateMap)stateSize++;
				termSize = inputAmount * 2 * stateSize;

				break;
			}
		}
		//output.kiss
		outputState = KISS;
		outfile_kiss << ".start_kiss" << endl;
		outfile_kiss << ".i " << inSize << endl;
		outfile_kiss << ".o " << outSize << endl;
		outfile_kiss << ".p " << termSize << endl;
		outfile_kiss << ".s " << stateSize << endl;
		outfile_kiss << ".r " << iniState << endl;
		for (int i = 0; i < stateSequence.size(); i++) {
			outfile_kiss << *stateMap[stateSequence[i]];
		}
		outfile_kiss << ".end_kiss" << endl;
		outfile_kiss.close();

		//output.dot
		outputState = DOT;
		outfile_dot << "digraph STG {" << endl;
		outfile_dot << "\trankdir = LR;" << endl << endl;
		outfile_dot << "\tINIT[shape = point];" << endl;
		for (int i = 0; i < stateSequence.size(); i++) {
			outfile_dot << "\t" << stateMap[stateSequence[i]]->getStatename() << "[label = \"" << stateMap[stateSequence[i]]->getStatename() << "\"];" << endl;
		}
		outfile_dot << endl;
		outfile_dot << "\tINIT -> " << iniState << ";" << endl;
		for (int i = 0; i < stateSequence.size(); i++) {
			outfile_dot << *stateMap[stateSequence[i]];
		}
		outfile_dot << "}" << endl;
		outfile_dot.close();

		cout << "state minimization ...complete" << endl;

	}
	else {
		cout << "wrong format!Please use:" << endl;
		cout << ">$ smin input.kiss output.kiss output.dot" << endl;
	}
}
