#-------------definition------------

CC = g++
DOT_png = dot -T png
TFILE = test
T_IN = input
T_OUT = output
SFILE = case
S_IN = case
S_OUT = case_output
run = ./smin


#--------------commands-------------

all: compile  test test_to_graph

compile: source.cpp
	$(CC) source.cpp -o smin -std=c++11 -lm

test: $(TFILE)1/$(T_IN)1.kiss $(TFILE)2/$(T_IN)2.kiss $(TFILE)3/$(T_IN)3.kiss
	$(run) $(TFILE)1/$(T_IN)1.kiss $(TFILE)1/$(T_OUT)1.kiss $(TFILE)1/$(T_OUT)1.dot
	mv $(T_IN).dot $(TFILE)1/$(T_IN).dot
	$(run) $(TFILE)2/$(T_IN)2.kiss $(TFILE)2/$(T_OUT)2.kiss $(TFILE)2/$(T_OUT)2.dot
	mv $(T_IN).dot $(TFILE)2/$(T_IN).dot
	$(run) $(TFILE)3/$(T_IN)3.kiss $(TFILE)3/$(T_OUT)3.kiss $(TFILE)3/$(T_OUT)3.dot
	mv $(T_IN).dot $(TFILE)3/$(T_IN).dot

test_to_graph: $(TFILE)1/$(T_IN).dot $(TFILE)1/$(T_OUT)1.dot $(TFILE)2/$(T_IN).dot $(TFILE)2/$(T_OUT)2.dot $(TFILE)3/$(T_IN).dot $(TFILE)3/$(T_OUT)3.dot
	$(DOT_png) $(TFILE)1/$(T_IN).dot > $(TFILE)1/$(T_IN)1.png
	$(DOT_png) $(TFILE)1/$(T_OUT)1.dot > $(TFILE)1/$(T_OUT)1.png
	$(DOT_png) $(TFILE)2/$(T_IN).dot > $(TFILE)2/$(T_IN)2.png
	$(DOT_png) $(TFILE)2/$(T_OUT)2.dot > $(TFILE)2/$(T_OUT)2.png
	$(DOT_png) $(TFILE)3/$(T_IN).dot > $(TFILE)3/$(T_IN)3.png
	$(DOT_png) $(TFILE)3/$(T_OUT)3.dot > $(TFILE)3/$(T_OUT)3.png

clean_all:
	rm $(TFILE)1/*.png $(TFILE)2/*.png $(TFILE)3/*.png
	rm $(TFILE)1/*.dot $(TFILE)2/*.dot $(TFILE)3/*.dot
	rm $(TFILE)1/$(T_OUT)1.kiss $(TFILE)2/$(T_OUT)2.kiss $(TFILE)3/$(T_OUT)3.kiss
clean_smin:
	rm smin

#--------------do samples-------------

sample: compile  cases case_to_graph

cases: $(SFILE)1/$(S_IN)1.kiss $(SFILE)2/$(S_IN)2.kiss $(SFILE)3/$(S_IN)3.kiss $(SFILE)4/$(S_IN)4.kiss $(SFILE)5/$(S_IN)5.kiss
	$(run) $(SFILE)1/$(S_IN)1.kiss $(SFILE)1/$(S_OUT)1.kiss $(SFILE)1/$(S_OUT)1.dot
	mv $(T_IN).dot $(SFILE)1/$(T_IN).dot
	$(run) $(SFILE)2/$(S_IN)2.kiss $(SFILE)2/$(S_OUT)2.kiss $(SFILE)2/$(S_OUT)2.dot
	mv $(T_IN).dot $(SFILE)2/$(T_IN).dot
	$(run) $(SFILE)3/$(S_IN)3.kiss $(SFILE)3/$(S_OUT)3.kiss $(SFILE)3/$(S_OUT)3.dot
	mv $(T_IN).dot $(SFILE)3/$(T_IN).dot
	$(run) $(SFILE)4/$(S_IN)4.kiss $(SFILE)4/$(S_OUT)4.kiss $(SFILE)4/$(S_OUT)4.dot
	mv $(T_IN).dot $(SFILE)4/$(T_IN).dot
	$(run) $(SFILE)5/$(S_IN)5.kiss $(SFILE)5/$(S_OUT)5.kiss $(SFILE)5/$(S_OUT)5.dot
	mv $(T_IN).dot $(SFILE)5/$(T_IN).dot

case_to_graph: $(SFILE)1/$(T_IN).dot $(SFILE)1/$(S_OUT)1.dot $(SFILE)2/$(T_IN).dot $(SFILE)2/$(S_OUT)2.dot $(SFILE)3/$(T_IN).dot $(SFILE)3/$(S_OUT)3.dot $(SFILE)4/$(T_IN).dot $(SFILE)4/$(S_OUT)4.dot $(SFILE)5/$(T_IN).dot $(SFILE)5/$(S_OUT)5.dot
	$(DOT_png) $(SFILE)1/$(T_IN).dot > $(SFILE)1/$(S_IN)1.png
	$(DOT_png) $(SFILE)1/$(S_OUT)1.dot > $(SFILE)1/$(S_OUT)1.png
	$(DOT_png) $(SFILE)2/$(T_IN).dot > $(SFILE)2/$(S_IN)2.png
	$(DOT_png) $(SFILE)2/$(S_OUT)2.dot > $(SFILE)2/$(S_OUT)2.png
	$(DOT_png) $(SFILE)3/$(T_IN).dot > $(SFILE)3/$(S_IN)3.png
	$(DOT_png) $(SFILE)3/$(S_OUT)3.dot > $(SFILE)3/$(S_OUT)3.png
	$(DOT_png) $(SFILE)4/$(T_IN).dot > $(SFILE)4/$(S_IN)4.png
	$(DOT_png) $(SFILE)4/$(S_OUT)4.dot > $(SFILE)4/$(S_OUT)4.png
	$(DOT_png) $(SFILE)5/$(T_IN).dot > $(SFILE)5/$(S_IN)5.png
	$(DOT_png) $(SFILE)5/$(S_OUT)5.dot > $(SFILE)5/$(S_OUT)5.png

clean_sample:
	rm $(SFILE)1/*.png $(SFILE)2/*.png $(SFILE)3/*.png $(SFILE)4/*.png $(SFILE)5/*.png
	rm $(SFILE)1/*.dot $(SFILE)2/*.dot $(SFILE)3/*.dot $(SFILE)4/*.dot $(SFILE)5/*.dot
	rm $(SFILE)1/$(S_OUT)1.kiss $(SFILE)2/$(S_OUT)2.kiss $(SFILE)3/$(S_OUT)3.kiss $(SFILE)4/$(S_OUT)4.kiss $(SFILE)5/$(S_OUT)5.kiss
