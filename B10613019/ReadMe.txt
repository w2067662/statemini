#you can simply use makefile commands to do all the instructions
Makefile commands:
>$ make all     # read input.kiss, and do state-minimization,
		# then output output.kiss output.dot (input.dot)
>$ make sample   # do state-minimization to sample cases


>$ make clean_all   # remove test output .DOT .PNG input.DOT files
>$ make clean_sample   # remove sample output .DOT .PNG input.DOT files


#Explanations
use the following command to create "smin" file:
>$ g++ source.cpp -o smin -std=c++11 -lm


execute smin file to read .KISS file and output .KISS and .DOT file:
>$ ./smin input.kiss output.kiss output.dot
